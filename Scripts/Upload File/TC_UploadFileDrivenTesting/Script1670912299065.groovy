import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('http://the-internet.herokuapp.com/upload')

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Upload/h3_file_uploaded'), 1)

myDir = System.getProperty('user.dir')

filePath = (myDir + '//Data Files//')

for(def rowNum = 1; rowNum <= findTestData('fileDataDriven').getRowNumbers(); rowNum++) {
	WebUI.uploadFile(findTestObject('Object Repository/Page_Upload/input_file'), filePath + findTestData('fileDataDriven').getValue(1, rowNum))
	
	WebUI.click(findTestObject('Page_Upload/btn_upload'))
	
	WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Upload/h3_file_uploaded'), 2)
	
	WebUI.back()
}

WebUI.closeBrowser()

