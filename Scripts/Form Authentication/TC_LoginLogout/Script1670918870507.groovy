import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('http://the-internet.herokuapp.com/login')

WebUI.verifyElementPresent(findTestObject('Page_Form_Authentication/h2_Login Page'), 1)

for (int i = 1; i <= findTestData('LoginDriven').getRowNumbers(); i++) {
    WebUI.setText(findTestObject('Page_Form_Authentication/input_username'), findTestData('LoginDriven').getValue(1, i))

    WebUI.setText(findTestObject('Page_Form_Authentication/input_password'), findTestData('LoginDriven').getValue(2, i))

    WebUI.click(findTestObject('Page_Form_Authentication/btn_Login'))

    if (WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Form_Authentication/notif_failed'), 1)) {
        assert true
    } else if (WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Form_Authentication/h2_Secure Area'), 1)) {
        WebUI.delay(3)

        WebUI.click.findTestObject('Object Repository/Page_Form_Authentication/a_Logout')
    } else {
        assert false
    }
}

WebUI.closeBrowser()

